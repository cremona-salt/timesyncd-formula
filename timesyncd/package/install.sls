# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import timesyncd with context %}

timesyncd-package-install-pkg-installed:
  pkg.installed:
    - name: {{ timesyncd.pkg.name }}
